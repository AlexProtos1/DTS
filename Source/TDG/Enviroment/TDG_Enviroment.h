// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDG/Interface/TDG_IGameActor.h"
#include "TDG\Weapon\TDG_StateEffect.h"
#include "TDG_Enviroment.generated.h"

UCLASS()
class TDG_API ATDG_Enviroment : public AActor, public ITDG_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDG_Enviroment();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	EPhysicalSurface GetSurfaceType() override;

	
	//Effect
	TArray<UTDG_StateEffect*> Effects;

	TArray<UTDG_StateEffect*> GetAllCurrentEffects() override;

	virtual void RemoveEffect(UTDG_StateEffect* RemovedEffect);
	virtual void AddEffect(UTDG_StateEffect* newEffect);
	

};