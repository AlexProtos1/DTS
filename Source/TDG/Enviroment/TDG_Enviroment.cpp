// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG/Enviroment/TDG_Enviroment.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"



// Sets default values
ATDG_Enviroment::ATDG_Enviroment()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDG_Enviroment::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDG_Enviroment::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDG_Enviroment::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent *myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial -> GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}
TArray<UTDG_StateEffect*> ATDG_Enviroment::GetAllCurrentEffects()
{
	return Effects;
}

void ATDG_Enviroment::RemoveEffect(UTDG_StateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);
}

void ATDG_Enviroment::AddEffect(UTDG_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}