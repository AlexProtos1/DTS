// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDGGameMode.h"
#include "TDGPlayerController.h"
#include "TDG/Character/TDGCharacter.h"
#include "UObject/ConstructorHelpers.h"
//
ATDGGameMode::ATDGGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDGPlayerController::StaticClass();

	DefaultPawnClass = ATDGCharacter::StaticClass();

	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));//Blueprint'/Game/Blueprint/Character/TopDownCharacter.TopDownCharacter'
	//if (PlayerPawnBPClass.Class != nullptr)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
}
