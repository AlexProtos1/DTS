// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG\Game\TDGGameInstance.h"

bool UTDGGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
    bool bIsFind = false;
    FWeaponInfo* WeaponInfoRow;
    if (WeaponInfoTable)
    {
        WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
      if (WeaponInfoRow)
       {
            bIsFind = true;
            OutInfo = *WeaponInfoRow;
       }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("UTDGGameInstance::GetWeaponInfoByTable - WeaponTable -NULL"));
    }
    WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
    
    return bIsFind;
}

bool UTDGGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
    bool bIsFind = false;

    if (DropItemInfoTable)
    {
        FDropItem* DropItemInfoRow;
        TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

        int8 i = 0;
        while (i < RowNames.Num() && !bIsFind)
        {
            DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
            if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
            {
                OutInfo = (*DropItemInfoRow);
                bIsFind = true;
            }
            i++;
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("UTDGGameInstance::GetDropItemInfoByTable - DropItemInfoTable -NULL"));
    }
    return bIsFind;
}
