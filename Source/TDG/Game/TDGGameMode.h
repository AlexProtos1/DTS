// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGGameMode.generated.h"

UCLASS(minimalapi)
class ATDGGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDGGameMode();
};



