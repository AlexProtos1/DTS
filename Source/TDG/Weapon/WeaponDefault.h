// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "TDG/FuncLibrary/Types.h"
#include "ProjectileDefault.h" // Bullet
#include "SleeveDrop.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadStart,UAnimMontage*, Anim, UStaticMesh*, WeaponMagazinMesh);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);



UCLASS()
class TDG_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponFireStart OnWeaponFireStart;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* SleeveDropLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAdditionalWeaponInfo WeaponInfo;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAnimationWeaponInfo AnimationWeaponInfo;*/

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Tick func
	virtual void Tick(float DeltaTime) override;
	void ClipDropTick(float DeltaTime);

//FireLogic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool bIsWeaponDropSleeves = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		FName IdWeaponName;
//ReloadLogic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		bool WeaponReloading = false;
	void CancelReload();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);

	//Fire
	bool CheckWeaponCanFire();
	void Fire();
	bool BlockFire = false;
	FVector GetFireEndLocation()const;

	//Projectile
	FProjectileInfo GetProjectile();
	int8 GetNumberProjectileByShoot() const;
	int32 GetWeaponRound();
	
	//WeaponSetting
	FVector ShootEndLocation = FVector(0);
	UFUNCTION(BlueprintCallable)
		void UpdateStateWeapon(EMovementState NewMovementState);
	void ChangeDispersion();
	
	//Drop
	bool DropClipFlag = false;
	float DropClipTimer = -1.0;
	UFUNCTION()
		void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	//Timers'flags
	float FireTimer = 0.0f;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);

	//Weapon Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;


	//Init func
	void InitReload();
	void WeaponInit();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
		float ReloadTime = 0.0f;
	
	void FinishReload();

	//Dispersion
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;
	float GetCurrentDispersion() const;
	void ChangeDispersionByShoot();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;

	//Sleeve
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "sleeve")
		TSubclassOf <class ASleeveDrop> Sleeve;
	UFUNCTION(BlueprintCallable)
		void DropSleeve();

	
	int GetAviableAmmoForReload();
	bool CheckCanWeaponReload();
	

};
