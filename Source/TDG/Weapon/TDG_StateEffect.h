// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "TDG_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDG_API UTDG_StateEffect : public UObject
{
	GENERATED_BODY()
public:

	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStackable = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TDG_API UTDGStateEffect_ExecuteOnce : public UTDG_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		float Power = 20.0f;

};

UCLASS()
class TDG_API UTDGStateEffect_ExecuteTimer : public UTDG_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();


	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		UParticleSystem* ParticleEffect = nullptr;
	UParticleSystemComponent* ParticleEmitter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		UNiagaraSystem* NiagaraEffect = nullptr;
	UNiagaraComponent* NiagaraEmmiter = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		bool bIsDamage = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float minDamage = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float InnerRad = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float OuterRad = 201.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute timer")
		float Falloff = 20.0f;


};

UCLASS()
class TDG_API UTDGStateEffect_SEET_Invulnerability : public UTDGStateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;

	void DestroyObject() override;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	bool bIsInv = false;
	

	virtual void ExecuteInvulnerability();
	
};
