// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG/Weapon/TDG_StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "TDG/Interface/TDG_IGameActor.h"
#include "TDG/Character/TDGHealthComponent.h"

bool UTDG_StateEffect::InitObject(AActor* Actor, FName NameBoneHit )
{
	myActor = Actor;
	ITDG_IGameActor* myInterface = Cast< ITDG_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;

}

void UTDG_StateEffect::DestroyObject()
{
	ITDG_IGameActor* myInterface = Cast<ITDG_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
	
}

//Once
bool UTDGStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce(); 
	return true;
}

void UTDGStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();

}

void UTDGStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDGHealthComponent* myHealthComponent = Cast<UTDGHealthComponent>(myActor->GetComponentByClass(UTDGHealthComponent::StaticClass()));
		if (myHealthComponent)
		{
			myHealthComponent->ChangeHealthValue(Power);
		}
		DestroyObject();
	}
}

	// -------- Timer ----------
bool UTDGStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDGStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDGStateEffect_ExecuteTimer::Execute, RateTime, true);
	
	if (ParticleEffect)
	{
		FName NameBoneToAttached = NameBoneHit;
		FVector Loc = FVector(0);
		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect,  myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		
		
	}



	return true;
}

void UTDGStateEffect_ExecuteTimer::DestroyObject()
{
	//---- for Emmiter -----
	if (ParticleEmitter)
	{
		//ParticleEmitter->
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	
	//---- for Niagara ----------
	if (NiagaraEmmiter)
	{
		NiagaraEmmiter->DestroyComponent();
		NiagaraEmmiter = nullptr;
	}
	

	Super::DestroyObject();
}

void UTDGStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTDGHealthComponent* myHealthComponent = Cast<UTDGHealthComponent>(myActor->GetComponentByClass(UTDGHealthComponent::StaticClass()));
		if (bIsDamage)
		{
			
			if (myHealthComponent && myHealthComponent->GetCurrentHealth() >= 0)
			{
				AActor* myChar = myHealthComponent->GetOwner();
				FVector Loc = myChar->GetActorLocation() + FVector(0.0f, 0.0f, 10.0f);
				TArray<AActor*> ActorToIgnore;
				ActorToIgnore.Add(myChar);

				UGameplayStatics::ApplyRadialDamageWithFalloff( GetWorld(),
					Power,
					Power,
					Loc,
					InnerRad,
					InnerRad,
					Falloff,
					NULL,
					ActorToIgnore,
					nullptr,
					nullptr
				);
			}
		}
		else
		{
			myHealthComponent->ChangeHealthValue(Power);
		}
		

		if (NiagaraEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);
			NiagaraEmmiter = UNiagaraFunctionLibrary::SpawnSystemAttached(NiagaraEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
}



//--------invulnerability--------

bool UTDGStateEffect_SEET_Invulnerability::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	UTDGHealthComponent* myHealthComponent = Cast<UTDGHealthComponent>(myActor->GetComponentByClass(UTDGHealthComponent::StaticClass()));
	if (myHealthComponent)
	{
		myHealthComponent->CanTakeDamage = false;
	}

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDGStateEffect_SEET_Invulnerability::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDGStateEffect_SEET_Invulnerability::ExecuteInvulnerability, RateTime, true);

	bIsInv = true;

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);


		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}


	return true;
}

void UTDGStateEffect_SEET_Invulnerability::ExecuteInvulnerability()
{
	if (myActor)
	{
		if (NiagaraEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);
			NiagaraEmmiter = UNiagaraFunctionLibrary::SpawnSystemAttached(NiagaraEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
}


void UTDGStateEffect_SEET_Invulnerability::DestroyObject()
{
	if (bIsInv)
	{
		UTDGHealthComponent* myHealthComponent = Cast<UTDGHealthComponent>(myActor->GetComponentByClass(UTDGHealthComponent::StaticClass()));
		myHealthComponent->CanTakeDamage = true;
		bIsInv = false;
	}

	//---- for Emmiter -----
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}

	//---- for Niagara ----------
	if (NiagaraEmmiter)
	{
		NiagaraEmmiter->DestroyComponent();
		NiagaraEmmiter = nullptr;
	}


	Super::DestroyObject();
}


