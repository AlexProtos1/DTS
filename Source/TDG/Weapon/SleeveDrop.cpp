// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG\Weapon\SleeveDrop.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TDG/FuncLibrary/Types.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ASleeveDrop::ASleeveDrop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SleeveCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	SleeveCollisionSphere->SetSphereRadius(3.f);

	RootComponent = SleeveCollisionSphere;

	SleeveCollisionSphere->bReturnMaterialOnMove = true;

	SleeveCollisionSphere->SetCanEverAffectNavigation(false);

	DropSleeveStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DropSleeveStaticMeshComponent"));
	DropSleeveStaticMeshComponent->SetupAttachment(RootComponent);
	DropSleeveStaticMeshComponent->SetGenerateOverlapEvents(false);
	
	
	

	SleeveMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	SleeveMovement->UpdatedComponent = RootComponent;
	SleeveMovement->InitialSpeed = 300.f;
	SleeveMovement->MaxSpeed = 300.f;


}

// Called when the game starts or when spawned
void ASleeveDrop::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASleeveDrop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

