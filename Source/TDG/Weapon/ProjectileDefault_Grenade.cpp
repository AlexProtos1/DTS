// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG\Weapon\ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	if (DrawDebug)
	{
		DrawDebugSphere(GetWorld(), this->GetActorLocation(), RadiusOfMaxDamage, 16.0f, FColor::Red, false, 10.f);
		DrawDebugSphere(GetWorld(), this->GetActorLocation(), RadiusOfMinDamage, 24.0f, FColor::Green, false, 10.f);
	}
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(3.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation(), 2);
	}

	TArray<AActor*> IgnoredActor;
	//FVector Z = FVector()
	FVector ExploseLocation = this->GetActorLocation() + FVector(0.0f, 0.0f, 50.0f);
	//bool isHit = 
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.5f,
		ExploseLocation,
		RadiusOfMaxDamage,
		RadiusOfMinDamage,
		ReduseDamage,
		NULL, IgnoredActor, this, nullptr);
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("hit: %s"), isHit));

	this->Destroy();
}

