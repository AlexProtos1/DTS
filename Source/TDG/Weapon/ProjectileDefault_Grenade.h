// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDG\Weapon\ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDG_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	void Explose();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TimeToExplose")
	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TimeToExplose")
		float TimeToExplose = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool DrawDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosionSettings")
		float RadiusOfMaxDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosionSettings")
		float RadiusOfMinDamage = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosionSettings")
		float ReduseDamage = 5.0f;

};
