// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG\Weapon\MagazinDrop.h"


// Sets default values
AMagazinDrop::AMagazinDrop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	
	MagazinMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Magazin"));
	MagazinMesh->SetGenerateOverlapEvents(false);
	MagazinMesh->SetCollisionProfileName(TEXT("BlockAll"));
	MagazinMesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AMagazinDrop::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMagazinDrop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

