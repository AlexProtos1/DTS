// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG\Character\TDGHealthComponent.h"



// Sets default values for this component's properties
UTDGHealthComponent::UTDGHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDGHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDGHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTDGHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDGHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTDGHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if (CanTakeDamage)
	{
		ChangeValue = ChangeValue * CoefDamage;
		Health += ChangeValue;
		OnHealthChange.Broadcast(Health, ChangeValue);
		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health < 0.0f)
			{
				OnDead.Broadcast();
			}
		}
	}
	
		

}

void UTDGHealthComponent::DeadEvent()
{

}

float UTDGHealthComponent::GetBulletDamage(AProjectileDefault* Projectile)
{
	return Projectile->Damage;
}
