// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDG\Character\TDGHealthComponent.h"
#include "TDGCharacterHealthComponent.generated.h"


/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TDG_API UTDGCharacterHealthComponent : public UTDGHealthComponent
{
	GENERATED_BODY()
public:
	FTimerHandle TimerHandle_CoolDownShield;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;
protected:
	float Shield = 100.0f;
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;
	void ChangeHealthValue(float ChangeValue) override;
	UFUNCTION(BlueprintCallable, Category = "Shield")
		float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();
	bool bIsNeedRecover = true;

	UFUNCTION(BlueprintCallable)
		float GetShieldValue();

};
