// Fill out your copyright notice in the Description page of Project Settings.


#include "TDG\Character\TDGCharacterHealthComponent.h"

void UTDGCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;
	
	if (Shield > 0.0f && ChangeValue < 0.0f && CanTakeDamage)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//add FX effects
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDGCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDGCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{

	Shield += ChangeValue;
	OnShieldChange.Broadcast(Shield, ChangeValue);
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}
	if (GetWorld() && CanTakeDamage && Health > 0)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShield, this, &UTDGCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
		
}

void UTDGCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld() && CanTakeDamage)
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDGCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
		
	}
	
}

void UTDGCharacterHealthComponent::RecoveryShield()
{
	if (bIsNeedRecover && CanTakeDamage)
	{
	float tmp = Shield;
		tmp = tmp + ShieldRecoveryValue;
		if (tmp > 100.0f)
		{
			Shield = 100.0f;
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			}
		}
		else
		{
			Shield = tmp;
		}
		OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
	}
	
}

float UTDGCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}
