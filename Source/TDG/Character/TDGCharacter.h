// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDG\FuncLibrary\Types.h"
#include "TDG\Weapon\WeaponDefault.h"
#include "TDG\Character\TDGInventoryComponent.h"
#include "TDG\Character\TDGCharacterHealthComponent.h"
#include "TDG\Interface\TDG_IGameActor.h"
#include "TDG\Weapon\TDG_StateEffect.h"
#include "TDGCharacter.generated.h"


UCLASS(Blueprintable)
class ATDGCharacter : public ACharacter, public ITDG_IGameActor
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

	//--------------Inputs-------------
	void InputAxisY(float Value);
	void InputAxisX(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();

	//ability
	void TryAbilityEnabled();
	
	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}
	// ------------------ Inputs End -----------------

	//--------- Input Flags ------------
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	
	bool SprintRunEnabled = false;
	bool WalkEnabled = false;
	bool AimEnabled = false;
	bool bIsAlive = true;
	
	EMovementState MovementState = EMovementState::Run_State;
	AWeaponDefault* CurrentWeapon = nullptr;
	UDecalComponent* CurrentCursor = nullptr;
	TArray<UTDG_StateEffect*> Effects;
	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
		void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount,
							struct FDamageEvent const& DamageEvent, 
							class AController* EventInstigator, 
							AActor* DamageCauser) override;

public:
	ATDGCharacter();

	FTimerHandle TimerHandle_RagdollTimer;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	virtual void Tick(float DeltaSeconds) override;


	
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDGInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDGCharacterHealthComponent* HealthComponent;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;


public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	// Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//-------------- Func -----------------
		void CharacterUpdate();
		void ChangeMovementState();

		void AttackCharEvent(bool bIsFiring);

	UFUNCTION()
		void InitWeapon(FName  IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION()
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim, UStaticMesh* WeaponMagazinMesh);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);
	void DropCurrentWeapon();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim, UStaticMesh* WeaponMagazinMesh);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDG_StateEffect> AbilityEffect;
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UTDG_StateEffect*> GetCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();


	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTDG_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDG_StateEffect* RemovedEffect) override;
	void AddEffect(UTDG_StateEffect* newEffect) override;
	//End Interface

	void NeedRecoverShield();

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetCharIsAlive();
};