// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TDG/TDG.h"
#include "TDG/Interface/TDG_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDG_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTDG_StateEffect* myEffect = Cast<UTDG_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossiableSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossiableSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossiableSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStackable)
					{
						int8 j = 0;
						TArray<UTDG_StateEffect*> CurrentEffects;
						ITDG_IGameActor* myInterface = Cast<ITDG_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}
						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
						
					}
					else
					{
						bIsCanAddEffect = true;
					}
					if (bIsCanAddEffect)
					{
						UTDG_StateEffect* NewEffect = NewObject<UTDG_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
					
				}
				i++;
			}
		}
		
	}
}