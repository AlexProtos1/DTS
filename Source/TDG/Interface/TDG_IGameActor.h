// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDG\Weapon\TDG_StateEffect.h"
#include "TDG\FuncLibrary\Types.h"
#include "TDG_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDG_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDG_API ITDG_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();

	//Effects
	virtual TArray<UTDG_StateEffect*> GetAllCurrentEffects();
	
	virtual void RemoveEffect(UTDG_StateEffect* RemovedEffect);
	virtual void AddEffect(UTDG_StateEffect* newEffect);
	

	//inventory
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};