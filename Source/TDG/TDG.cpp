// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDG/TDG.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDG, "TDG" );

DEFINE_LOG_CATEGORY(LogTDG)
 